import flask
import difflib
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity



app = flask.Flask(__name__, template_folder='templates')

df2 = pd.read_csv('./model/tmdb.csv')

count = CountVectorizer(stop_words='english')
count_matrix = count.fit_transform(df2['industry'])

cosine_sim2 = cosine_similarity(count_matrix, count_matrix)

df2 = df2.reset_index()
indices = pd.Series(df2.index, index=df2['title'])
all_titles = [df2['title'][i] for i in range(len(df2['title']))]

def get_recommendations(title):
    cosine_sim = cosine_similarity(count_matrix, count_matrix)
    idx = indices[title]
    sim_scores = list(enumerate(cosine_sim[idx]))
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    sim_scores = sim_scores[1:11]
    company_indices = [i[0] for i in sim_scores]
    tit = df2['title'].iloc[company_indices]
    dat = df2['founding_date'].iloc[company_indices]
    datt = df2['vote_average'].iloc[company_indices]
    seed = df2['seed_fund'].iloc[company_indices]
    type = df2['industry'].iloc[company_indices]
    contact = df2['homepage'].iloc[company_indices]
    
    return_df = pd.DataFrame(columns=['Title','Year','rating'])
    return_df['Title'] = tit
    return_df['Year'] = dat
    return_df['rating'] = datt
    return_df['seed_fund'] = seed
    return_df['industry'] = type
    return_df['homepage'] = contact
    return return_df

# Set up the main route
@app.route('/', methods=['GET', 'POST'])

def main():
    if flask.request.method == 'GET':
        return(flask.render_template('index.html'))
            
    if flask.request.method == 'POST':
        m_name = flask.request.form['company_name']
        m_name = m_name.title()
#        check = difflib.get_close_matches(m_name,all_titles,cutout=0.50,n=1)
        if m_name not in all_titles:
            return(flask.render_template('negative.html',name=m_name))
        else:
            result_final = get_recommendations(m_name)
            names = []
            dates = []
            rate = []
            seedf = []
            ctype =[]
            site = []
            
            for i in range(len(result_final)):
                names.append(result_final.iloc[i][0])
                dates.append(result_final.iloc[i][1])
                rate.append(result_final.iloc[i][2])
                seedf.append(result_final.iloc[i][3])
                ctype.append(result_final.iloc[i][4])
                site.append(result_final.iloc[i][5])

                

            return flask.render_template('positive.html',company_names=names,founding_date=dates,vote_average=rate,seed_fund=seedf,industry=ctype,homepage=site,search_name=m_name)

if __name__ == '__main__':
    app.run()